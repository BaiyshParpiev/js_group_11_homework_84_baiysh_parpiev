const express = require('express');
const User = require('../models/User');
const router = express.Router();

router.post('/', async(req, res) => {
    if(!req.body.username || !req.body.password) {
        return res.status(404).send('Data not valid');
    }

    const {username} = req.body;

    const existingUser = await User.findOne({username});
    if(existingUser){
        return res.status(404).send({message: 'User already exist'});
    }

    const userData = {
        username: req.body.username,
        password: req.body.password
    }

    const user = new User(userData);
    try{
        user.generateToken();
        await user.save();
        res.send(user);
    }catch{
        res.sendStatus(500);
    }
});

router.post('/session', async(req, res) => {
    const user = await User.findOne({username: req.body.username});

    if(!user){
        return res.status(401).send('Username not found');
    }

    const isMatch = await user.checkPassword(req.body.password);

    if(!isMatch){
        return res.status(401).send('Password is wrong');
    }

    try{
        user.generateToken();
        await user.save();
        res.send({token: user.token});
    }catch{
        res.sendStatus(500);
    }

});

module.exports = router;
