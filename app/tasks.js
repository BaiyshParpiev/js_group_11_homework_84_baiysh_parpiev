const express = require('express');
const Task = require('../models/Task');
const auth = require('../middleware/auth');
const mongoose = require('mongoose');

const router = express.Router();

router.get('/', auth, async(req, res) => {
    try{
        const tasks = await Task.find().populate('user', 'username');
        res.send(tasks);
    }catch{
        res.sendStatus(500);
    }
});

router.post('/', auth, async(req, res) => {
    if(!req.body.title || !req.body.description || !req.body.status){
        return res.status(400).send('Data not valid');
    }

    const taskData = {
        user: req.user._id,
        title: req.body.title,
        description: req.body.description,
        status: req.body.status,
    };


    const task = new Task(taskData);

    try{
        await task.save();
        res.send(task)
    }catch{
        res.sendStatus(500)
    }

});

router.put('/:id', auth, async(req, res) => {
    const { id } = req.params;
    const { title, description, status } = req.body;
    if (!mongoose.Types.ObjectId.isValid(id)) return res.status(404).send(`No post with id: ${id}`);
    const task = await Task.findById(id);
    if(!task.user._id.equals(req.user._id)){
        return res.status(404).send({error: 'It is not your task'})
    }
    const updatedPost = { title, description, status, _id: id };
    try{
        await Task.findByIdAndUpdate(id, updatedPost, { new: true });
        res.send(updatedPost);
    }catch{
        res.sendStatus(500);
    }
})

router.delete('/:id', auth, async(req, res) => {
    const task = await Task.findById(req.params.id);
    if(!task.user._id.equals(req.user._id)){
        return res.status(404).send({error: 'It is not your task'})
    }

    try{
        const task = await Task.findByIdAndDelete(req.params.id);
        if(task){
            res.end(`Task ${task.title} removed`)
        }else{
            res.status(404).send({error: 'Task not found'})
        }
    }catch{
        res.sendStatus(500);
    }
});

module.exports = router;

