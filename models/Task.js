const mongoose = require('mongoose');
const idValidator = require('mongoose-id-validator');

const TaskSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    description:{
        type: String,
        required: true,
    },
    status: {
        type: String,
        enum: ['new', 'in_progress', 'complete'],
        required: true,
    },
});

TaskSchema.plugin(idValidator);

const Task = mongoose.model('Task', TaskSchema);


module.exports = Task;


